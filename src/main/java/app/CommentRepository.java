package app;

import java.util.UUID;
import org.springframework.data.repository.CrudRepository;

public interface CommentRepository extends CrudRepository<CommentEntity, UUID> {

  CommentEntity findOneById(UUID id);

  Iterable<CommentEntity> findByEstablishmentId(UUID id);
}
