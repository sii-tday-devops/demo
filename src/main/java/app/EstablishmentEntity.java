package app;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Entity
@Data
public class EstablishmentEntity {

  @Id
  @GeneratedValue(generator = "uuid2")
  @GenericGenerator(name = "uuid2", strategy = "uuid2")
  @Column(name = "id", updatable = false)
  @Type(type = "uuid-char")
  @ApiModelProperty(notes = "The generated ID", accessMode = AccessMode.READ_ONLY, hidden = true)
  @Setter(AccessLevel.NONE)
  private UUID id;

  @Column(nullable = false, length = 255)
  private String name;

  @Column(nullable = true, columnDefinition = "text")
  private String address;

  @Column(nullable = true, length = 255)
  private String position;

  @Column(nullable = true, length = 255)
  private String phone;

  @ElementCollection private List<String> links;

  @ElementCollection private List<String> pictures;

  @Column(updatable = false)
  @ApiModelProperty(accessMode = AccessMode.READ_ONLY, hidden = true)
  private Date createdAt;

  @Column
  @ApiModelProperty(accessMode = AccessMode.READ_ONLY, hidden = true)
  @Setter(AccessLevel.NONE)
  private Date updatedAt;

  @PrePersist
  void createdAt() {
    this.createdAt = this.updatedAt = new Date();
  }

  @PreUpdate
  void updatedAt() {
    this.updatedAt = new Date();
  }
}
