package app;

import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.NotAcceptableStatusException;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping(path = "/api/comments", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
public class CommentController {

  @Autowired private CommentRepository commentRepository;

  @Autowired private EstablishmentRepository establishmentRepository;

  @GetMapping
  public Iterable<CommentEntity> list(@RequestParam("establishment") UUID establishmentId) {
    return commentRepository.findByEstablishmentId(establishmentId);
  }

  @PostMapping
  public ResponseEntity<CommentEntity> create(
      @RequestBody CommentEntity entity, UriComponentsBuilder ucBuilder) {
    log.trace("Creating new comment.");

    if (establishmentRepository.findOneById(entity.getEstablishmentId()) == null) {
      throw new NotAcceptableStatusException("Establishment not found.");
    }

    try {
      commentRepository.save(entity);

      HttpHeaders headers = new HttpHeaders();
      headers.setLocation(
          ucBuilder.path("/api/comments/{id}").buildAndExpand(entity.getId()).toUri());

      log.info("Comment created with id {}.", entity.getId());

      return new ResponseEntity<CommentEntity>(entity, headers, HttpStatus.CREATED);
    } catch (Exception e) {
      log.error("Unable to create comment.", e);
      throw new NotAcceptableStatusException("Unable to create comment.");
    }
  }

  @GetMapping(value = "/{id}")
  public ResponseEntity<CommentEntity> get(@PathVariable("id") UUID id) {
    CommentEntity entity = commentRepository.findOneById(id);

    if (entity == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Comment not found.");
    }

    return new ResponseEntity<CommentEntity>(entity, HttpStatus.OK);
  }

  @PutMapping(value = "/{id}")
  public ResponseEntity<CommentEntity> update(
      @PathVariable UUID id, @RequestBody CommentEntity updated) {
    log.trace("Updating comment {}.", id);

    CommentEntity entity = commentRepository.findOneById(id);

    if (entity == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Comment not found.");
    }

    entity.setAuthor(updated.getAuthor());
    entity.setContent(updated.getContent());

    commentRepository.save(entity);

    log.info("Comment with id {} updated.", id);

    return new ResponseEntity<CommentEntity>(entity, HttpStatus.ACCEPTED);
  }

  @DeleteMapping(value = "/{id}")
  public ResponseEntity<CommentEntity> delete(@PathVariable("id") UUID id) {
    log.trace("Deleting comment {}.", id);

    CommentEntity entity = commentRepository.findOneById(id);

    if (entity == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Comment not found.");
    }

    commentRepository.delete(entity);

    log.info("Comment with id {} deleted.", id);

    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}
