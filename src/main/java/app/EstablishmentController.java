package app;

import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.NotAcceptableStatusException;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping(path = "/api/establishment", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
public class EstablishmentController {

  @Autowired private EstablishmentRepository repository;

  @GetMapping
  public Iterable<EstablishmentEntity> list() {
    return repository.findAll();
  }

  @PostMapping
  public ResponseEntity<EstablishmentEntity> create(
      @RequestBody EstablishmentEntity entity, UriComponentsBuilder ucBuilder) {
    log.trace("Creating new establishment.");

    try {
      repository.save(entity);

      HttpHeaders headers = new HttpHeaders();
      headers.setLocation(
          ucBuilder.path("/api/establishment/{id}").buildAndExpand(entity.getId()).toUri());

      log.info("Establishment created with id {}.", entity.getId());

      return new ResponseEntity<EstablishmentEntity>(entity, headers, HttpStatus.CREATED);
    } catch (Exception e) {
      log.error("Unable to create establishment.", e);
      throw new NotAcceptableStatusException("Unable to create establishment.");
    }
  }

  @GetMapping(value = "/{id}")
  public ResponseEntity<EstablishmentEntity> get(@PathVariable("id") UUID id) {
    EstablishmentEntity entity = repository.findOneById(id);

    if (entity == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Establishment not found.");
    }

    return new ResponseEntity<EstablishmentEntity>(entity, HttpStatus.OK);
  }

  @PutMapping(value = "/{id}")
  public ResponseEntity<EstablishmentEntity> update(
      @PathVariable UUID id, @RequestBody EstablishmentEntity updated) {
    log.trace("Updating establishment {}.", id);

    EstablishmentEntity entity = repository.findOneById(id);

    if (entity == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Establishment not found.");
    }

    entity.setName(updated.getName());
    entity.setAddress(updated.getAddress());
    entity.setLinks(updated.getLinks());
    entity.setPhone(updated.getPhone());
    entity.setPictures(updated.getPictures());
    entity.setPosition(updated.getPosition());

    repository.save(entity);

    log.info("Establishment with id {} updated.", id);

    return new ResponseEntity<EstablishmentEntity>(entity, HttpStatus.ACCEPTED);
  }

  @DeleteMapping(value = "/{id}")
  public ResponseEntity<EstablishmentEntity> delete(@PathVariable("id") UUID id) {
    log.trace("Deleting establishment {}.", id);

    EstablishmentEntity entity = repository.findOneById(id);

    if (entity == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Establishment not found.");
    }

    repository.delete(entity);

    log.info("Establishment with id {} deleted.", id);

    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}
