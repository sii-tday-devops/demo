package app;

import java.util.UUID;
import org.springframework.data.repository.CrudRepository;

public interface EstablishmentRepository extends CrudRepository<EstablishmentEntity, UUID> {

  EstablishmentEntity findOneById(UUID id);
}
